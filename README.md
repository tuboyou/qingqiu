# qingqiu

#### 介绍
使用requests的简单接口测试框架
建议结合pytest使用

#### 软件架构
软件架构说明


#### 安装教程

```
pip3 install qingqiu --upgrade
```

#### 使用说明

##### 基本使用
```
from qingqiu.qing import qing

qiu =qing().setMethod("get").setURL('https://xxxx').request()
qiu.checkStatusCode(200)
qiu.checkHeader("Content-Type","application/json")
# qiu.Json 可以校验结果
qiu.checkResultJson('''
{"code":0,"message":"成功",
"namedemo":"type:int|None|str|bool|float|dict|list|tuple #用于判断值的类型 value:{}=='xxxx' #对值进行校验{}为响应的值,支持简单python语句 格式说明:必须是type:来指定类型,不写默认是str;必须是value:之后写python语句校验;不写默认完全匹配校验"
,"data":[{"key":"val"},{"说明":"list的值,当队列只有一个时,循环匹配每个;当列表没有值时,不校验;当队列长度超过1个时,全量匹配校验"}]}
''')
```
##### class使用
```
from qingqiu.qing import qing
class Test_xxx:
    method='get'
    scheme="https" #不写默认 https
    host="xxx.xxx"
    path="/index"
    params="a=1&b=2"
    def test_场景名称(self):
        qiu=qing(self).request()
        qiu.checkStatusCode(200)
        qiu.checkHeader("Content-Type","application/json")
        assert qiu.Json['code']==1
```






